package de.htw_berlin.gma.techmobTest;


import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenuItem;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;
import org.robolectric.shadows.ShadowListView;

import android.content.Intent;
import android.os.Build;
import android.view.MenuItem;
import android.widget.ListView;

import de.htw_berlin.gma.techmob.BuildConfig;
import de.htw_berlin.gma.techmob.MainActivity;
import de.htw_berlin.gma.techmob.R;
import de.htw_berlin.gma.techmob.SortedList;
import de.htw_berlin.gma.techmob.models.Badestellen;

/**
 * Created by Haled on 13.07.2016.
 */
@Config(constants = BuildConfig.class, sdk = Build.VERSION_CODES.LOLLIPOP)
@RunWith(RobolectricGradleTestRunner.class)
public class MainActivityTest {

    private MainActivity test;
    private ListView listView;

    @Before
    public void setup() {
        test = Robolectric.setupActivity(MainActivity.class);//"Starte" die MainActivity
        listView = (ListView) test.findViewById(R.id.badeListe);
    }

    //Testet ob Einträge existieren und ob diese stimmen
    @Test
    public void badelistTest() {

        for (Badestellen badestelle : test.badestellenList) {
            if (badestelle.getBadName().equalsIgnoreCase("Alter Hof")) {
                assertEquals("Alter Hof", badestelle.getBadName());
                assertNotNull(badestelle);
            }
        }
    }

    //Testet ob in Der List etwas vorhanden ist
    @Test
    public void listNotEmpty() {
        assertNotNull(test.badestellenList);
    }

    //Testet ob die größe der Liste mit der größe der ListView übereinstimmen
    @Test
    public void lisViewTest() {
        ShadowListView shadowListView = Shadows.shadowOf(listView);
        shadowListView.populateItems();
        int rowCount = listView.getChildCount();
        int listeCount = test.badestellenList.size();
        assertEquals(rowCount, listeCount);//listViewSize == ListSize?
    }

    //Testet ob man zur richtigen Activity weitergeleitet wird
    @Test
    public void onOptionsItemSelectedNextIntentTest() {
        ShadowActivity shadowActivity = Shadows.shadowOf(test);
        MenuItem item = new RoboMenuItem(R.id.action_charlotten);
        test.onOptionsItemSelected(item);
        Intent startedIntent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = Shadows.shadowOf(startedIntent);
        assertThat(shadowIntent.getComponent().getClassName(), equalTo(SortedList.class.getName()));
    }
}
