package de.htw_berlin.gma.techmob;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.htw_berlin.gma.techmob.models.Badestellen;

public class SortedList extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ListView listView;
    private List<Badestellen> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(de.htw_berlin.gma.techmob.R.layout.activity_sorted_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(de.htw_berlin.gma.techmob.R.id.listViewSorted);
        Intent i = getIntent();
        list = (List<Badestellen>) i.getSerializableExtra("list");
        listView.setOnItemClickListener(this);
        BadestellenAdapter adapter = null;
        adapter = new BadestellenAdapter(this, de.htw_berlin.gma.techmob.R.layout.row, list);
        listView.setAdapter(adapter);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Intent intent = createIntentwithExtra(v, position);
        startActivity(intent);
    }

    public Intent createIntentwithExtra(View v, int position) {
        Intent intent = new Intent(getApplicationContext(), Description.class);

        Badestellen listTemp = list.get(position);
        intent.putExtra("position", listTemp);
        return intent;
    }

    public class BadestellenAdapter extends ArrayAdapter {

        private List<Badestellen> badeListe;
        private int resource;
        private LayoutInflater inflater;

        public BadestellenAdapter(Context context, int resource, List<Badestellen> objects) {
            super(context, resource, objects);
            badeListe = objects;
            this.resource = resource;
            inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(resource, null);
            }

            TextView textName;
            TextView textBezirk;
            TextView textTemp;


            textName = (TextView) convertView.findViewById(de.htw_berlin.gma.techmob.R.id.textName);
            textBezirk = (TextView) convertView.findViewById(de.htw_berlin.gma.techmob.R.id.textBezirk);
            textTemp = (TextView) convertView.findViewById(de.htw_berlin.gma.techmob.R.id.textTemp);
            textName.setText(badeListe.get(position).getBadName());
            textBezirk.setText(badeListe.get(position).getBezirk());
            textTemp.setText("Temperatur: " + badeListe.get(position).getTemperatur());

            return convertView;
        }
    }
}
