package de.htw_berlin.gma.techmob;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

import de.htw_berlin.gma.techmob.models.Badestellen;

public class Description extends AppCompatActivity {
    private TextView textName;
    private TextView textBezirk;
    private TextView textDatum;
    private TextView textSicht;
    private TextView textQuali;
    private TextView textTemp;
    private ImageView view;
    private Badestellen bad = new Badestellen();
    private static final int zahlStat = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(de.htw_berlin.gma.techmob.R.layout.activity_description);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        textName = (TextView) findViewById(de.htw_berlin.gma.techmob.R.id.badName);
        textBezirk = (TextView) findViewById(de.htw_berlin.gma.techmob.R.id.bezirkName);
        textDatum = (TextView) findViewById(de.htw_berlin.gma.techmob.R.id.datum);
        textSicht = (TextView) findViewById(de.htw_berlin.gma.techmob.R.id.sicht);
        textQuali = (TextView) findViewById(de.htw_berlin.gma.techmob.R.id.qualitaet);
        textTemp = (TextView) findViewById(de.htw_berlin.gma.techmob.R.id.temperatur);
        view = (ImageView) findViewById(de.htw_berlin.gma.techmob.R.id.imageView);

        Intent i = getIntent();
        bad = (Badestellen) i.getSerializableExtra("position");
        textName.setText(bad.getBadName());
        textSicht.setText(bad.getSicht());
        textTemp.setText(bad.getTemperatur());
        textDatum.setText(bad.getDatum());
        textQuali.setText(bad.getQualitaet());
        textBezirk.setText(bad.getBezirk());

        Random rand = new Random();
        int zahl = 0;

        zahl = (rand.nextInt(zahlStat)) + 1;
        System.out.println(zahl);

        switch (zahl) {
            case 1:
                view.setImageResource(de.htw_berlin.gma.techmob.R.drawable.alterhof);
                break;
            case 2:
                view.setImageResource(de.htw_berlin.gma.techmob.R.drawable.bammelecke);
                break;
            case 3:
                view.setImageResource(de.htw_berlin.gma.techmob.R.drawable.breitehorn);
                break;
            case 4:
                view.setImageResource(de.htw_berlin.gma.techmob.R.drawable.buergerablage);
                break;
            case 5:
                view.setImageResource(de.htw_berlin.gma.techmob.R.drawable.daemeritzsee);
                break;
            default:
                view.setImageResource(de.htw_berlin.gma.techmob.R.drawable.ploetzensee);
                break;
        }
    }
}


