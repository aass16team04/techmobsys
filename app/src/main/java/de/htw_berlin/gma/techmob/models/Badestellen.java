package de.htw_berlin.gma.techmob.models;


import java.io.Serializable;

/**
 * Created by basha on 11.07.2016.
 */
public class Badestellen implements Serializable {


    private String badName;
    private String bezirk;
    private String datum;
    private String temperatur;
    private String sicht;
    private String qualitaet;

    public String getBadName() {
        return badName;
    }

    public void setBadName(String badName) {
        this.badName = badName;
    }

    public String getBezirk() {
        return bezirk;
    }

    public void setBezirk(String bezirk) {
        this.bezirk = bezirk;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getTemperatur() {
        return temperatur;
    }

    public void setTemperatur(String temperatur) {
        this.temperatur = temperatur;
    }

    public String getSicht() {
        return sicht;
    }

    public void setSicht(String sicht) {
        this.sicht = sicht;
    }

    public String getQualitaet() {
        return qualitaet;
    }

    public void setQualitaet(String qualitaet) {
        this.qualitaet = qualitaet;
    }
}
