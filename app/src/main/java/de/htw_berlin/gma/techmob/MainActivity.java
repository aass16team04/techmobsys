package de.htw_berlin.gma.techmob;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import de.htw_berlin.gma.techmob.models.Badestellen;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView listView;
    private ArrayList<Badestellen> steglitzList = new ArrayList<>();
    private ArrayList<Badestellen> spandauList = new ArrayList<>();
    private ArrayList<Badestellen> treptowList = new ArrayList<>();
    private ArrayList<Badestellen> reinickList = new ArrayList<>();
    private ArrayList<Badestellen> charlottenList = new ArrayList<>();
    private ArrayList<Badestellen> lichtenbergList = new ArrayList<>();
    private ArrayList<Badestellen> mitteList = new ArrayList<>();
    private ArrayList<Badestellen> pankowList = new ArrayList<>();
    public List<Badestellen> badestellenList = new ArrayList<>();

    static final String TAG = MainActivity.class.getSimpleName();
    private static final String FILENAME = TAG + ".txt";
    private static String ABSOLUTE_FILENAME;


    private final String URL_TO_HIT = "http://www.berlin.de/lageso/gesundheit/gesundheitsschutz/badegewaesser/liste-der-badestellen/index.php/index/all.json?q=";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.badeListe);
        listView.setOnItemClickListener(this);
        new JSONTask().execute(URL_TO_HIT);


        /*Hier wird die Liste aus der Datei gelesen, falls die badestellenList ist nach dem JSONTask null ist
          falls sie nicht null ist bedeutet dass das der JSONTask erfolgreich war und es eine Internetverbindung besteht
         */
        if (badestellenList == null) {
            JSONArray array = loadJson();
            badestellenList = jsonIntoList(array);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        Intent intent = createIntentwithExtra(v, position);
        startActivity(intent);
    }

    public Intent createIntentwithExtra(View v, int position) {
        Intent intent = new Intent(getApplicationContext(), Description.class);

        Badestellen listTemp = badestellenList.get(position);
        intent.putExtra("position", listTemp);
        return intent;
    }


    public class JSONTask extends AsyncTask<String, String, List<Badestellen>> implements Serializable {

        @Override
        protected List<Badestellen> doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                String finalJson = buffer.toString();
                JSONObject pJsonObject = new JSONObject(finalJson);
                JSONArray pJsonObjectJSONArray = pJsonObject.getJSONArray("index");

                String jsonString = pJsonObjectJSONArray.toString();
                SharedPreferences preferences = getSharedPreferences("json", Context.MODE_WORLD_WRITEABLE);
                SharedPreferences.Editor prEditor = getSharedPreferences("json", Context.MODE_WORLD_WRITEABLE).edit();
                prEditor.putString("jsonData", jsonString);
                prEditor.commit();

                badestellenList = jsonIntoList(pJsonObjectJSONArray);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            for (Badestellen badestelle : badestellenList) {
                if (badestelle.getBezirk().equals("Treptow-Köpenick")) {
                    treptowList.add(badestelle);
                } else if (badestelle.getBezirk().equals("Steglitz-Zehlendorf")) {
                    steglitzList.add(badestelle);
                } else if (badestelle.getBezirk().equals("Spandau")) {
                    spandauList.add(badestelle);
                } else if (badestelle.getBezirk().equals("Reinickendorf")) {
                    reinickList.add(badestelle);
                } else if (badestelle.getBezirk().equals("Charlottenburg-Wilmersdorf")) {
                    charlottenList.add(badestelle);
                } else if (badestelle.getBezirk().equals("Lichtenberg")) {
                    lichtenbergList.add(badestelle);
                } else if (badestelle.getBezirk().equals("Mitte")) {
                    mitteList.add(badestelle);
                } else if (badestelle.getBezirk().equals("Pankow")) {
                    pankowList.add(badestelle);
                }
            }

            if (badestellenList.size() < 5) {
                return null;
            }
            return badestellenList;
        }

        @Override
        protected void onPostExecute(List<Badestellen> result) {
            super.onPostExecute(result);
            BadestellenAdapter adapter = null;
            adapter = new BadestellenAdapter(getApplicationContext(), de.htw_berlin.gma.techmob.R.layout.row, result);
            result = badestellenList;
            listView.setAdapter(adapter);
        }
    }


    public List<Badestellen> jsonIntoList(JSONArray jsonArray) {
        List<Badestellen> liste = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject fJsonObject = jsonArray.getJSONObject(i);
                Badestellen badestellen = new Badestellen();
                badestellen.setBadName(fJsonObject.getString("badname"));
                badestellen.setBezirk(fJsonObject.getString("bezirk"));
                badestellen.setDatum(fJsonObject.getString("dat"));
                badestellen.setSicht(fJsonObject.getString("sicht"));
                badestellen.setQualitaet(fJsonObject.getString("wasserqualitaet"));
                badestellen.setTemperatur(fJsonObject.getString("temp"));
                liste.add(badestellen);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return liste;
    }


    public JSONArray loadJson() {
        String jsonString;
        JSONArray jsonObject;
        SharedPreferences preferences = getSharedPreferences("json", Context.MODE_WORLD_WRITEABLE);
        jsonString = preferences.getString("jsonData", "0");
        if (jsonString != null) {
            try {
                jsonObject = new JSONArray(jsonString);
                return jsonObject;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }
        return null;
    }

    public class BadestellenAdapter extends ArrayAdapter {

        private List<Badestellen> badeListe;
        private int resource;
        private LayoutInflater inflater;
        private TextView textName;
        private TextView textBezirk;
        private TextView textTemp;

        public BadestellenAdapter(Context context, int resource, List<Badestellen> objects) {
            super(context, resource, objects);
            badeListe = objects;
            this.resource = resource;
            inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = inflater.inflate(resource, null);
            }

            textName = (TextView) convertView.findViewById(de.htw_berlin.gma.techmob.R.id.textName);
            textBezirk = (TextView) convertView.findViewById(de.htw_berlin.gma.techmob.R.id.textBezirk);
            textTemp = (TextView) convertView.findViewById(de.htw_berlin.gma.techmob.R.id.textTemp);
            textName.setText(badeListe.get(position).getBadName());
            textBezirk.setText(badeListe.get(position).getBezirk());
            textTemp.setText("Temperatur: " + badeListe.get(position).getTemperatur());

            return convertView;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(de.htw_berlin.gma.techmob.R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == de.htw_berlin.gma.techmob.R.id.action_refresh) {
            new JSONTask().execute(URL_TO_HIT);
            return true;
        } else if (id == de.htw_berlin.gma.techmob.R.id.action_charlotten) {
            Intent intent = new Intent(this, SortedList.class);
            intent.putExtra("list", charlottenList);
            startActivity(intent);
        } else if (id == de.htw_berlin.gma.techmob.R.id.action_lichtenberg) {
            Intent intent = new Intent(this, SortedList.class);
            intent.putExtra("list", lichtenbergList);
            startActivity(intent);
        } else if (id == de.htw_berlin.gma.techmob.R.id.action_mitte) {
            Intent intent = new Intent(this, SortedList.class);
            intent.putExtra("list", mitteList);
            startActivity(intent);
        } else if (id == de.htw_berlin.gma.techmob.R.id.action_pankow) {
            Intent intent = new Intent(this, SortedList.class);
            intent.putExtra("list", pankowList);
            startActivity(intent);
        } else if (id == de.htw_berlin.gma.techmob.R.id.action_steglitz) {
            Intent intent = new Intent(this, SortedList.class);
            intent.putExtra("list", steglitzList);
            startActivity(intent);
        } else if (id == de.htw_berlin.gma.techmob.R.id.action_treptow) {
            Intent intent = new Intent(this, SortedList.class);
            intent.putExtra("list", treptowList);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
